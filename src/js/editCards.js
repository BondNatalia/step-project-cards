import {openModal, closeModal, formAddCard} from "./addCards.js";
import {createButton} from "./app.js";
import {
    doctorSelect,
    visitTarget,
    visitDescription,
    visitUrgency,
    patientName,
    patientSurname,
    patientFathersName,
    patientPressure,
    patientMass,
    patientDisease,
    patientAge,
    patientLastVisit,
    visitStatus
} from "./addCards.js";
import {token} from "./app.js";
import {createCard} from "./createCards.js";
import {onSubmitForm, onClick} from "./app.js";

const editButton = document.querySelector(".btn-edit-card");


document.body.addEventListener("click", ev => {
    const btnEdit = ev.target.parentElement;
    const editedCard = btnEdit.id;
    if (btnEdit.classList.contains('btn-edit')) {
        editButton.dataset.id = editedCard;
        getCard(editedCard);
        openModal();
        createButton.classList.add("invisible");
        editButton.classList.remove("invisible");

    }
});

async function getCard(id) {
    let response = await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });
    let cardToEdit = await response.json();
    doctorSelect.value = cardToEdit.doctorSelect;
    visitTarget.value=cardToEdit.target;
    visitDescription.value=cardToEdit.description;
        visitUrgency.value=cardToEdit.urgency;
        patientName.value=cardToEdit.patientName;
        patientSurname.value=cardToEdit.patientSurname;
        patientFathersName.value=cardToEdit.patientFathersName;
        patientPressure.value=cardToEdit.patientPressure;
        patientMass.value=cardToEdit.patientMass;
        patientDisease.value=cardToEdit.patientDisease;
        patientAge.value=cardToEdit.patientAge;
        patientLastVisit.value=cardToEdit.patientLastVisit;
        visitStatus.value=cardToEdit.visitStatus;
}


editButton.addEventListener('click', () => {
    const editButtonID = editButton.dataset.id;

    putToServer(editButtonID);
})
editButton.addEventListener('submit', onSubmitForm);
editButton.addEventListener('click', onClick);

async function putToServer(id) {
    let response = await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
            doctorSelect: `${doctorSelect.value}`,
            target: `${visitTarget.value}`,
            description: `${visitDescription.value}`,
            urgency: `${visitUrgency.value}`,
            patientName: `${patientName.value}`,
            patientSurname: `${patientSurname.value}`,
            patientFathersName: `${patientFathersName.value}`,
            patientPressure: `${patientPressure.value}`,
            patientMass: `${patientMass.value}`,
            patientDisease: `${patientDisease.value}`,
            patientAge: `${patientAge.value}`,
            patientLastVisit: `${patientLastVisit.value}`,
            visitStatus: `${visitStatus.value}`,
        })
    });
    let updatedCard = await response.json();
    let oldCard = document.getElementById(`${updatedCard.id}`);
    oldCard.remove();
    createCard(updatedCard);
    formAddCard.reset();
    closeModal();

}
