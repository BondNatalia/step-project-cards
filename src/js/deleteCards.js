import {token} from "./app.js";
import {checkCards} from "./checkCards.js";

document.body.addEventListener("click", ev => {
    const btnDelete = ev.target;
    if (btnDelete.classList.contains('delete')){
        deleteCard(btnDelete.id)
    }
});

export async function deleteCard(id) {
    await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}/`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${token}`
        },
    })
    const cardToDelete = document.getElementById(`${id}`);
    cardToDelete.remove();
    checkCards()
}