

document.body.addEventListener("click", ev => {
    const btnShowMore = ev.target.parentElement;
    if (btnShowMore.classList.contains('btn-show-more')){
        btnShowMore.nextElementSibling.classList.remove("show-more");
        btnShowMore.classList.remove('btn-show-more');
        btnShowMore.classList.add("shown");
    }
    else if (btnShowMore.classList.contains('shown')){
        btnShowMore.nextElementSibling.classList.add("show-more");
        btnShowMore.classList.remove('shown');
        btnShowMore.classList.add('btn-show-more');
    }
});
