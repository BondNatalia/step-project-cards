import { token, modal, btnVisit, closeButton, createButton, } from "./app.js";
import { createCard } from "./createCards.js";
import {formValidate} from "./formValidate.js";

export const formAddCard = document.getElementById('formAddCArd');
// Функция для открытия модального окна

export function openModal() {
    modal.style.display = "flex";
}
// Функция для закрытия модального окна
export function closeModal() {
    modal.style.display = "none";
}
// Добавляем обработчик события на кнопку "Создать визит"
btnVisit.addEventListener('click', openModal);

// Добавляем обработчик события на кнопку закрытия модального окна

closeButton.addEventListener('click',closeModal);

// Функция для отображения полей для записи к выбранному врачу
const doctorSelected = document.getElementById('doctor-select');
doctorSelected.addEventListener('change', showFields);

function showFields() {
    const selectedValue = doctorSelected.value;
    // Скрываем все поля
    const fields = document.querySelectorAll('#cardiologist-fields, #dentist-fields, #therapist-fields');
    fields.forEach(field => {
        field.style.display = 'none';
    });

    // Отображаем поля для выбранного врача
    const selectedFields = document.querySelector(`#${selectedValue}-fields`);
    selectedFields.style.display = 'block';
}
////отправка данных на сервер
createButton.addEventListener('click', postToServer);

export const doctorSelect = document.querySelector("#doctor-select");
export const visitTarget = document.querySelector("#target");
export const visitDescription = document.querySelector("#description");
export const visitUrgency = document.querySelector("#visit-urgency");
export const patientName = document.querySelector("#name");
export const patientSurname = document.querySelector("#surname");
export const patientFathersName = document.querySelector("#fathersName");
export const patientPressure = document.querySelector("#normal-pressure");
export const patientMass = document.querySelector("#body-mass-index");
export const patientDisease = document.querySelector("#cardio-diseases");
export const patientAge = document.querySelector("#age");
export const patientLastVisit = document.querySelector("#last-visit");
export const visitStatus = document.querySelector("#status-select");

async function postToServer() {
    let error = formValidate(formAddCard);
    let response = await fetch("https://ajax.test-danit.com/api/v2/cards", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
            doctorSelect: `${doctorSelect.value}`,
            target: `${visitTarget.value}`,
            description: `${visitDescription.value}`,
            urgency: `${visitUrgency.value}`,
            patientName: `${patientName.value}`,
            patientSurname: `${patientSurname.value}`,
            patientFathersName: `${patientFathersName.value}`,
            patientPressure: `${patientPressure.value}`,
            patientMass: `${patientMass.value}`,
            patientDisease: `${patientDisease.value}`,
            patientAge: `${patientAge.value}`,
            patientLastVisit: `${patientLastVisit.value}`,
            visitStatus: `${visitStatus.value}`
        })
    });
    let newCard = await response.json();
    createCard(newCard);
    formAddCard.reset();
    closeModal();

}
