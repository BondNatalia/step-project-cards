import {token} from "./app.js";
import {checkCards} from "./checkCards.js";

export async function getCards() {
    let response = await fetch("https://ajax.test-danit.com/api/v2/cards", {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });
    let cardsList = await response.json();
    cardsList.forEach(item => createCard(item))
}

export function createCard(card) {
        const {
            doctorSelect, target, id, description, urgency, patientName, patientSurname,
            patientFathersName, patientPressure, patientMass, patientDisease, patientAge, patientLastVisit, visitStatus
        } = card;
        if (doctorSelect === "dentist") {
            const dentistVisit = new VisitDentist(id, doctorSelect, patientName, patientSurname,
                patientFathersName, target, description, urgency, visitStatus, patientLastVisit);
            dentistVisit.render()
        } else if (doctorSelect === "cardiologist") {
            const cardiologistVisit = new VisitCardiologist(id, doctorSelect, patientName, patientSurname,
                patientFathersName, target, description, urgency, visitStatus, patientPressure, patientMass, patientDisease, patientAge);
            cardiologistVisit.render()
        } else if (doctorSelect === "therapist") {
            const therapistVisit = new VisitTherapist(id, doctorSelect, patientName, patientSurname,
                patientFathersName, target, description, urgency, visitStatus, patientAge);
            therapistVisit.render()
        }
    checkCards();
    }






const visits = document.querySelector(".cards");

class Visit {
    constructor(id, doctor, patientSurname, name, patientFathersName, target, description, urgency, visitStatus) {
        this.id = id;
        this.doctor = doctor;
        this.patientSurname = patientSurname;
        this.name = name;
        this.patientFathersName = patientFathersName;
        this.target = target;
        this.description = description;
        this.urgency = urgency;
        this.visitStatus = visitStatus;
    }
}

class VisitDentist extends Visit {
    constructor(id, doctor, patientSurname, name, patientFathersName, target, description, urgency, visitStatus, lastVisit) {
        super(id, doctor, patientSurname, name, patientFathersName, target, description, urgency, visitStatus)
        this.lastVisit = lastVisit;
        this.title = "Визит к стоматологу"
    }

    render() {
        visits.insertAdjacentHTML("beforeend",
            `<div id="${this.id}" class="card-item visible-flex" data-status="${this.visitStatus}" data-urgency = "${this.urgency}" > 
                <div>
                        <p>
                            <label>ФИО:</label>
                            <span>${this.patientSurname}</span>
                            <span>${this.name}</span>
                            <span>${this.patientFathersName}</span>
                        </p> 
                         <p>
                         
                             <span>${this.title}</span>
                         </p> 
                            <button  class="btn-show-more btn-icon">
                                <img src="./dist/img/more.png" >
                            </button>
                        <div class="show-more" >
                        <p>
                            <label>Цель визита:</label>
                            <span>${this.target}</span>
                        </p>
                        <p>
                            <label>Oписание визита:</label>
                            <span>${this.description}</span>
                        </p> 
                        <p>
                            <label>Срочность визита:</label>
                            <span>${this.urgency}</span>
                        </p> 
                        <p>
                            <label>Дата последнего посещения:</label>
                            <span>${this.lastVisit}</span>
                        </p>
                        <p>
                            <label>Статус визита</label>
                            <span>${this.visitStatus}</span>
                        </p>
                    </div>
                </div>
                    <div class="btn-item">
                        <button id=${this.id} class="btn-icon btn-edit">
                            <img class="small" src="./dist/img/edit.png">
                        </button>
                        <button class="btn-icon btn-delete-card">
                            <img id=${this.id} class="delete" src="./dist/img/icon-delete-green.png">
                        </button>
                    </div>
            </div>`
        )
    }
}


class VisitCardiologist extends Visit {
    constructor(id, doctor, patientSurname, name, patientFathersName, target, description, urgency, visitStatus, pressure, massIndex, patientDisease, age) {
        super(id, doctor, patientSurname, name, patientFathersName, target, description, urgency, visitStatus)
        this.pressure = pressure;
        this.massIndex = massIndex;
        this.patientDesease = patientDisease;
        this.age = age;
        this.title = "Визит к кардиологу"
    }

    render() {

        visits.insertAdjacentHTML("beforeend",
            `<div id="${this.id}" class="card-item visible-flex" data-status="${this.visitStatus}" data-urgency = "${this.urgency}" > 
                <div>
                    <p>
                        <label>ФИО:</label>
                        <span>${this.patientSurname}</span>
                        <span>${this.name}</span>
                        <span>${this.patientFathersName}</span>
                    </p> 
                    <p>
                 <span>${this.title}</span>
                   </p> 
                    <button  class="btn-show-more btn-icon">
                        <img src="./dist/img/more.png" >
                    </button>
                    <div class="show-more" >
                        <p>
                            <label>Цель визита:</label>
                            <span>${this.target}</span>
                        </p>
                        <p>
                            <label>Oписание визита:</label>
                            <span>${this.description}</span>
                        </p> 
                        <p>
                            <label>Срочность визита:</label>
                            <span>${this.urgency}</span>
                        </p> 
                       
                        <p>
                            <label>Обычное давление:</label>
                            <span>${this.pressure}</span>
                        </p>
                        <p>
                            <label>Индекс массы тела:</label>
                            <span>${this.massIndex}</span>
                        </p> 
                        <p>
                            <label>Перенесенные заболевания сердечно-сосудистой системы:</label>
                            <span>${this.patientDesease}</span>
                        </p> 
                        <p>
                            <label>Возраст:</label>
                            <span>${this.age}</span>
                        </p>
                        <p>
                            <label>Статус визита</label>
                            <span>${this.visitStatus}</span>
                        </p>
                </div>
                </div>
                <div class="btn-item">
                    <button id=${this.id} class="btn-icon btn-edit">
                        <img class="small" src="./dist/img/edit.png">
                </button>
                    <button class="btn-icon btn-delete-card">
                        <img id=${this.id} class="delete" src="./dist/img/icon-delete-green.png">
                    </button>
                </div>
            </div>`
        )
    }
}

class VisitTherapist extends Visit {
    constructor(id, doctor, patientSurname, name, patientFathersName, target, description, urgency, visitStatus, age) {
        super(id, doctor, patientSurname, name, patientFathersName, target, description, urgency, visitStatus)
        this.age = age;
        this.title = "Визит к терапевту";
    }

    render() {
        visits.insertAdjacentHTML("beforeend",
            `<div id="${this.id}" class="card-item visible-flex" data-status="${this.visitStatus}" data-urgency = "${this.urgency}" > 
            <div>
                <p>
                    <label>ФИО:</label>
                    <span>${this.patientSurname}</span>
                    <span>${this.name}</span>
                    <span>${this.patientFathersName}</span>
                </p> 
                 <p>
                    
                     <span>${this.title}</span>
                 </p> 
                <button  class="btn-show-more btn-icon">
                    <img src="./dist/img/more.png">
                </button>
                <div class="show-more"  >
                    <p>
                        <label>Цель визита:</label>
                        <span>${this.target}</span>
                    </p>
                    <p>
                        <label>Oписание визита:</label>
                        <span>${this.description}</span>
                    </p> 
                    <p>
                        <label>Срочность визита:</label>
                        <span>${this.urgency}</span>
                    </p> 
                    <p>
                        <label>Возраст:</label>
                        <span>${this.age}</span>
                    </p>
                    <p>
                            <label>Статус визита</label>
                            <span>${this.visitStatus}</span>
                        </p>
                </div>
            </div>
            <div class="btn-item">
                    <button class="btn-icon btn-edit" id="${this.id}">
                        <img class="small" src="./dist/img/edit.png">
                    </button>
                    <button  class="btn-icon btn-delete-card">
                        <img id=${this.id} class="delete" src="./dist/img/icon-delete-green.png">
                    </button>
                </div>
            </div>`
        )
    }

}