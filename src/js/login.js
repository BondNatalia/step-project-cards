import {btnLogin, btnVisit, email, login, modalLogin, password, users} from "./app.js";
import {getCards} from "./createCards.js";
import {checkCards} from "./checkCards.js";

btnLogin.addEventListener('click', () => {
    modalLogin.style.display = "flex";
})
login.addEventListener("click", checkLogin);
let success;
function checkLogin() {
    users.forEach(el => {
        const inputEmail = email.value;
        const inputPassword = password.value
        if (el.email === inputEmail && el.password === inputPassword) {
            success = true;
            modalLogin.style.display = "none";
        }
    })
    loginSuccess(success);

}


function loginSuccess(success) {
    if (success) {
        btnLogin.classList.add("invisible");
        btnVisit.classList.remove("invisible");
        getCards();


    } else {
        alert("Неправильный логин или пароль");
    }
}