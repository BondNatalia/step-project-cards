import {onClick} from "./app.js";

const searchButton = document.querySelector(".btn-search");
searchButton.addEventListener('click', onClick);
searchButton.addEventListener("click", search);

function search() {
    const allCards = Array.from(document.querySelectorAll(".card-item"));
    const searchWord = document.querySelector("#inputSearch").value.toLowerCase();
    const status = document.querySelector("#status");
    const selectedStatus = status.value.toLowerCase();
    const urgency = document.querySelector("#urgency");
    const selectedUrgency = urgency.value.toLowerCase();
    allCards.forEach(card => {
        card.classList.add("invisible");
        card.classList.remove("visible-flex");
        if ((card.textContent.toLowerCase().includes(searchWord) || searchWord===undefined )
            && (card.dataset.status.toLowerCase() === selectedStatus || selectedStatus === "all")
            && (card.dataset.urgency.toLowerCase() === selectedUrgency || selectedUrgency === "all")) {
            card.classList.remove("invisible");
            card.classList.add("visible-flex");
        }
    });
    }
